package com.demo.employees.entity;

import java.util.List;

import lombok.Data;

@Data
public class EmployeeResponse {
	
	private String status;
	private List<Employee> data;
	
}
