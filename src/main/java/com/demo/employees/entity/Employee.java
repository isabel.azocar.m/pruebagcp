package com.demo.employees.entity;

import lombok.Data;

@Data
public class Employee {

	private String id;
	private String employee_name;
	private String employee_salary;
	private String employee_age;
	private String profile_image;

}
