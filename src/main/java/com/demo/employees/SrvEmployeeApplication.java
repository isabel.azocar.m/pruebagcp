package com.demo.employees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class SrvEmployeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SrvEmployeeApplication.class, args);
	}

	/*
	 * @Bean public PubSubInboundChannelAdapter
	 * messageChannelAdapter(@Qualifier("myInputChannel") MessageChannel
	 * inputChannel, PubSubTemplate pubSubTemplate) { PubSubInboundChannelAdapter
	 * adapter = new PubSubInboundChannelAdapter(pubSubTemplate, subscription);
	 * adapter.setOutputChannel(inputChannel);
	 * 
	 * return adapter; }
	 * 
	 * @Bean public MessageChannel myInputChannel() { return new DirectChannel(); }
	 * 
	 * @ServiceActivator(inputChannel = "myInputChannel") public void
	 * messageReceiver(String payload) { log.info("Message arrived! Payload: " +
	 * payload);
	 * 
	 * }
	 */
}
