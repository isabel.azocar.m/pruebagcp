package com.demo.employees.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.demo.employees.entity.EmployeeResponse;
import com.google.gson.Gson;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmployeeServiceimpl implements EmployeeService {

	@Value("${url.api.employees}")
	private String apiEmployeesURL;

	@Value("${file.path}")
	private String filePath;

	@Value("${host.sftp}")
	private String hostSftp;

	@Value("${port.sftp}")
	private int portSftp;

	@Value("${user.sftp}")
	private String userSftp;

	@Value("${pass.sftp}")
	private String passSftp;

	@Value("${directory.sftp}")
	private String directorySftp;

	@Autowired
	RestTemplate restTemplate;

	@Override
	public String exportEmployeesToCSV() {
		String respuesta = null;
		HttpEntity<String> req = null;
		Gson json = new Gson();
		ResponseEntity<EmployeeResponse> resp = null;

		resp = restTemplate.exchange(apiEmployeesURL, HttpMethod.GET, req, EmployeeResponse.class);
		
		try {

			String path = writeCsvFromEmployee(json.toJson(resp.getBody()));
			send(path);
			respuesta = "Archivo enviado con éxito";
		} catch (Exception e) {
			log.error(e.getMessage());
			respuesta = "Error al enviar archivo";
		}

		return respuesta;

	}

	public String writeCsvFromEmployee(String jsonEmployees) throws Exception {

		JSONObject output;
		String path = null;
		System.out.println(jsonEmployees);

		try {
			output = new JSONObject(jsonEmployees);
			JSONArray employees = output.getJSONArray("data");

			File file = new File(filePath);
			path = file.getAbsolutePath();
			System.out.println("PATH: " + path);
			String csv = CDL.toString(employees);
			FileUtils.writeStringToFile(file, csv, "UTF-8");
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return path;
	}

	public void send(String nombreArchivo) {
		String host = hostSftp;
		int port = portSftp;
		String user = userSftp;
		String pass = passSftp;
		String directorio = directorySftp;

		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		try {
			JSch jsch = new JSch();
			session = jsch.getSession(user, host, port);
			session.setPassword(pass);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(directorio);
			File f = new File(nombreArchivo);
			channelSftp.put(new FileInputStream(f), f.getName());
			log.info("Archivo transferido con éxito");
		} catch (Exception ex) {
			log.error(ex.getMessage());
		} finally {

			channelSftp.exit();
			channel.disconnect();
			session.disconnect();
		}
	}

	
}
