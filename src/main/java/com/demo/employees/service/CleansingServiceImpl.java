package com.demo.employees.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.pubsub.support.AcknowledgeablePubsubMessage;
import org.springframework.stereotype.Service;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CleansingServiceImpl implements CleansingService {

	@Value("${PUBSUB_SUBSCRIPTION}")
	private String subscription;

	
	@Autowired
	PubSubTemplate pubSubTemplate;

	@Override
	public String cleanEmployees() {

		int maxMessages = 10;
		boolean returnImmediately = false;
		List<AcknowledgeablePubsubMessage> messages = pubSubTemplate.pull(subscription, maxMessages, returnImmediately);

		pubSubTemplate.ack(messages);

		messages.forEach(message -> {
			try {
				String resp = obtenerBody(message.getPubsubMessage().getData().toStringUtf8());
				String path = writeCsvFromEmployee(resp);
				send(path);
			} catch (Exception e) {
				log.error(e.getMessage());
			}
		});
		
		return "Archivo creado en SFTP";

	}

	public String obtenerBody(String mensaje) throws JSONException {
		JSONObject jsonObject = new JSONObject(mensaje);
		JSONArray body = new JSONArray(jsonObject.getString("body"));

		JSONObject employee = new JSONObject();
		JSONArray datos = new JSONArray();
		JSONObject respuesta = new JSONObject();

		for (int i = 0; i < body.length(); i++) {
			employee = (JSONObject) body.get(i);
			employee.remove("employee_name");
			datos.put(employee);
		}

		respuesta.put("data", datos);

		return respuesta.toString();
	}

	public String writeCsvFromEmployee(String jsonEmployees) throws Exception {

		JSONObject output;
		String path = null;
		System.out.println(jsonEmployees);

		try {
			output = new JSONObject(jsonEmployees);
			JSONArray employees = output.getJSONArray("data");

			File file = new File("CleansingEmployees.csv");
			path = file.getAbsolutePath();
			System.out.println("PATH: " + path);
			String csv = CDL.toString(employees);
			FileUtils.writeStringToFile(file, csv, "UTF-8");
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return path;
	}

	public void send(String nombreArchivo) {
		String host = "sftp-employees-service.employees";
		int port = 22;
		String user = "foo";
		String pass = "pass";
		String directorio = "/upload";

		Session session = null;
		Channel channel = null;
		ChannelSftp channelSftp = null;
		try {
			JSch jsch = new JSch();
			session = jsch.getSession(user, host, port);
			session.setPassword(pass);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(directorio);
			File f = new File(nombreArchivo);
			channelSftp.put(new FileInputStream(f), f.getName());
			log.info("Archivo transferido con éxito");
		} catch (Exception ex) {
			log.error(ex.getMessage());
		} finally {

			channelSftp.exit();
			channel.disconnect();
			session.disconnect();
		}
	}

}
