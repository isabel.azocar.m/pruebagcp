package com.demo.employees.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.demo.employees.service.CleansingService;
import com.demo.employees.service.EmployeeService;

@RestController
public class EmployeeController {

	@Autowired
	private EmployeeService employeeServiceImpl;

	@Autowired
	private CleansingService cleansingServiceImpl;

	@RequestMapping(value = "/employees", method = RequestMethod.GET)
	public ResponseEntity<?> exportEmployees() throws IOException {

		try {
			String respuesta = employeeServiceImpl.exportEmployeesToCSV();
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@RequestMapping(value = "/cleansing", method = RequestMethod.GET)
	public ResponseEntity<?> cleanEmployees() throws IOException {

		try {
			String respuesta = cleansingServiceImpl.cleanEmployees();
			return new ResponseEntity<>(respuesta, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
