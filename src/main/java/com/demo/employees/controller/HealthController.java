package com.demo.employees.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {

	@RequestMapping(value = "/healthCheck", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity getLoginUsuario() {
		return new ResponseEntity(HttpStatus.OK);
	}

}
