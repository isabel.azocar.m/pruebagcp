FROM openjdk:8u252-jdk-slim-buster
EXPOSE 8080
WORKDIR /app
RUN mkdir -p /export-employees
ARG JAR_FILE=target/srv-employees.jar
ADD ${JAR_FILE} /srv-employees.jar
ENTRYPOINT exec java -Djava.security.egd=file:/dev/./urandom -Duser.timezone=America/Santiago -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -jar /srv-employees.jar